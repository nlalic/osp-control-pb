from lib.page   import Page
from lib.switch import Switch

class Controller(object):
    def __init__(self, view, switch):
        self.switch = switch
        self.view   = view
        if self.switch.connected:
            self.view.connect.address = self.switch.tcp_address
            self.view.connect.setConnected()
            self.view.controls.throw_position = switch.throw_position
            self.view.page = Page.CONTROLS
        else:
            self.view.connect.address = ''
            self.view.connect.setDisconnected()
            self.view.controls.throw_position = 1
            self.view.page = Page.CONNECT
        self.view.connect.connect_clicked.connect(self.handleConnect)
        self.view.controls.throwPositionChanged.connect(self.handleThrowPositionChanged)

    def handleConnect(self):
        self.view.error.clear()
        if not self.view.connect.hasAcceptableInput():
            self.view.error.showMessage('*Enter valid IP address')
            return
        address = self.view.connect.address
        try:
            self.switch.open_tcp(address)
            if not self.switch.connected:
                self.view.error.showMessage('*Could not connect')
                return
        except Exception as err:
            self.view.error.showMessage(f'*Connect error: {err}')
            return
        # success
        self.view.page = Page.CONTROLS
    def handleThrowPositionChanged(self, position):
        self.view.error.clear()
        if not self.switch.connected:
            self.view.error.showMessage('*Not connected')
            return
        try:
            self.switch.throw_position = position
        except Exception as err:
            self.view.error.showMessage(f'*Error setting switch: {err}')
            return
        try:
            self.view.throw_position = self.switch.throw_position
        except Exception as err:
            self.view.error.showMessage(f'Error reading switch: {err}')
