# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtcreator/connect.ui',
# licensing of 'qtcreator/connect.ui' applies.
#
# Created: Thu Jul 18 11:11:50 2019
#      by: pyside2-uic  running on PySide2 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Connect(object):
    def setupUi(self, Connect):
        Connect.setObjectName("Connect")
        self.verticalLayout = QtWidgets.QVBoxLayout(Connect)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget_2 = QtWidgets.QWidget(Connect)
        self.widget_2.setObjectName("widget_2")
        self.formLayout = QtWidgets.QFormLayout(self.widget_2)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.addressLabel = QtWidgets.QLabel(self.widget_2)
        self.addressLabel.setObjectName("addressLabel")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.addressLabel)
        self.address = IPAddressLineEdit(self.widget_2)
        self.address.setObjectName("address")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.address)
        self.verticalLayout.addWidget(self.widget_2)
        self.widget = QtWidgets.QWidget(Connect)
        self.widget.setObjectName("widget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.widget)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(285, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.connect = QtWidgets.QPushButton(self.widget)
        self.connect.setObjectName("connect")
        self.horizontalLayout.addWidget(self.connect)
        self.verticalLayout.addWidget(self.widget)

        self.retranslateUi(Connect)
        QtCore.QMetaObject.connectSlotsByName(Connect)

    def retranslateUi(self, Connect):
        self.addressLabel.setText(QtWidgets.QApplication.translate("Connect", "IP Address:", None, -1))
        self.connect.setText(QtWidgets.QApplication.translate("Connect", "Connect", None, -1))

from qtrf.widgets import IPAddressLineEdit
