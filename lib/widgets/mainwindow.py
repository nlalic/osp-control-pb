from lib.widgets.ui_mainwindow import Ui_MainWindow
from PySide2.QtWidgets         import QMainWindow

class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.connect.address = ''
        self.connect.setDisconnected()
        self.controls.throw_position = 1

    @property
    def page(self):
        return Page(self.page_index)
    @page.setter
    def page(self, value):
        self.page_index = int(value)

    @property
    def page_index(self):
        return self.ui.pages.currentIndex()
    @page_index.setter
    def page_index(self, value):
        self.ui.pages.setCurrentIndex(value)

    @property
    def connect(self):
        return self.ui.connect
    @property
    def controls(self):
        return self.ui.controls
    @property
    def error(self):
        return self.ui.error
