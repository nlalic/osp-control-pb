# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtcreator/controls.ui',
# licensing of 'qtcreator/controls.ui' applies.
#
# Created: Thu Jul 18 09:42:01 2019
#      by: pyside2-uic  running on PySide2 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_Controls(object):
    def setupUi(self, Controls):
        Controls.setObjectName("Controls")
        self.verticalLayout = QtWidgets.QVBoxLayout(Controls)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.dial = QtWidgets.QDial(Controls)
        self.dial.setMinimum(1)
        self.dial.setMaximum(6)
        self.dial.setProperty("value", 1)
        self.dial.setTracking(False)
        self.dial.setWrapping(False)
        self.dial.setNotchTarget(1.0)
        self.dial.setNotchesVisible(True)
        self.dial.setObjectName("dial")
        self.verticalLayout.addWidget(self.dial)
        self.widget_3 = QtWidgets.QWidget(Controls)
        self.widget_3.setObjectName("widget_3")
        self.formLayout_2 = QtWidgets.QFormLayout(self.widget_3)
        self.formLayout_2.setContentsMargins(0, 0, 0, 0)
        self.formLayout_2.setObjectName("formLayout_2")
        self.label = QtWidgets.QLabel(self.widget_3)
        self.label.setObjectName("label")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.spinBox = QtWidgets.QSpinBox(self.widget_3)
        self.spinBox.setKeyboardTracking(False)
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(6)
        self.spinBox.setObjectName("spinBox")
        self.formLayout_2.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.spinBox)
        self.verticalLayout.addWidget(self.widget_3)

        self.retranslateUi(Controls)
        QtCore.QMetaObject.connectSlotsByName(Controls)

    def retranslateUi(self, Controls):
        self.label.setText(QtWidgets.QApplication.translate("Controls", "Throw Position:", None, -1))

