from lib.widgets.ui_connect import Ui_Connect
from PySide2.QtWidgets      import QWidget


class Connect(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_Connect()
        self.ui.setupUi(self)
        self.setDisconnected()
        self.ui.connect.clicked.connect(lambda: print('connect clicked'))

    def hasAcceptableInput(self):
        return self.ui.address.hasAcceptableInput()

    @property
    def address(self):
        return self.ui.address.text()
    @address.setter
    def address(self, value):
        self.ui.address.setText(value)

    @property
    def is_connected(self):
        return self._is_connected

    @property
    def connect_clicked(self):
        return self.ui.connect.clicked

    def setConnected(self):
        self._is_connected = True
        self.ui.address.setDisabled(True)
        self.ui.connect.setText('Disconnect')
    def setDisconnected(self):
        self._is_connected = False
        self.ui.address.setEnabled(True)
        self.ui.connect.setText('Connect')
