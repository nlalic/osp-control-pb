from PySide2.QtCore    import Qt
from PySide2.QtGui     import QBrush, QPalette
from PySide2.QtWidgets import QLabel
from threading         import Timer

MESSAGE_LIFETIME_S = 5.0

class ErrorLabel(QLabel):
    def __init__(self, parent=None):
        super(ErrorLabel, self).__init__(parent)
        palette = QPalette()
        palette.setBrush(QPalette.WindowText, QBrush(Qt.red))
        self.setPalette(palette)
        self.timer = None

    def showMessage(self, message):
        self.clear()
        self.setText(message)
        self.start_timer()

    def start_timer(self):
        self.timer = Timer(MESSAGE_LIFETIME_S, self.clear)
        self.timer.start()
    def clear_timer(self):
        if not self.timer:
            return
        self.timer.cancel()
        self.timer = None
    def clear(self):
        QLabel.clear(self)
        self.clear_timer()
