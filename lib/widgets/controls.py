from lib.widgets.ui_controls import Ui_Controls
from PySide2.QtCore          import Signal
from PySide2.QtWidgets       import QWidget


class Controls(QWidget):
    throwPositionChanged = Signal(int)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.ui = Ui_Controls()
        self.ui.setupUi(self)
        self._throw_position = 1
        self.ui.dial.setValue(1)
        self.ui.spinBox.setValue(1)
        self.ui.dial   .valueChanged.connect(self._handleDialValueChanged)
        self.ui.spinBox.valueChanged.connect(self._handleSpinBoxValueChanged)

    @property
    def throw_position(self):
        return self._throw_position
    @throw_position.setter
    def throw_position(self, position):
        if position == self.throw_position:
            return
        self._throw_position = position
        dial_blocked     = self.ui.dial   .blockSignals(True)
        spin_box_blocked = self.ui.spinBox.blockSignals(True)
        self.ui.dial   .setValue(position)
        self.ui.spinBox.setValue(position)
        self.ui.dial   .blockSignals(dial_blocked)
        self.ui.spinBox.blockSignals(spin_box_blocked)
        self.throwPositionChanged.emit(position)

    def _handleDialValueChanged(self, value):
        self.throw_position = value
    def _handleSpinBoxValueChanged(self, value):
        self.throw_position = value
