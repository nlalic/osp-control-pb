# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtcreator/mainwindow.ui',
# licensing of 'qtcreator/mainwindow.ui' applies.
#
# Created: Thu Jul 18 11:14:56 2019
#      by: pyside2-uic  running on PySide2 5.13.0
#
# WARNING! All changes made in this file will be lost!

from PySide2 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(400, 300)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.centralWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pages = QtWidgets.QStackedWidget(self.centralWidget)
        self.pages.setObjectName("pages")
        self.connectPage = QtWidgets.QWidget()
        self.connectPage.setObjectName("connectPage")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.connectPage)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.connect = Connect(self.connectPage)
        self.connect.setObjectName("connect")
        self.verticalLayout_2.addWidget(self.connect)
        spacerItem = QtWidgets.QSpacerItem(20, 237, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem)
        self.pages.addWidget(self.connectPage)
        self.controlsPage = QtWidgets.QWidget()
        self.controlsPage.setObjectName("controlsPage")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.controlsPage)
        self.verticalLayout_3.setSpacing(0)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.controls = Controls(self.controlsPage)
        self.controls.setObjectName("controls")
        self.verticalLayout_3.addWidget(self.controls)
        spacerItem1 = QtWidgets.QSpacerItem(20, 237, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.pages.addWidget(self.controlsPage)
        self.verticalLayout.addWidget(self.pages)
        self.error = ErrorLabel(self.centralWidget)
        self.error.setObjectName("error")
        self.verticalLayout.addWidget(self.error)
        MainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtWidgets.QApplication.translate("MainWindow", "MainWindow", None, -1))

from lib.widgets.connect import Connect
from lib.widgets.controls import Controls
from lib.widgets.error_label import ErrorLabel
