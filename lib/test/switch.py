import socket

class Switch(object):
    def __init__(self, instrument, module, switch):
        self._connected      = False
        self._tcp_address    = '0.0.0.0'
        self._tcp_port       = 0
        self._throw_position = 1
        self._errors         = []

    @property
    def connected(self):
        return self._connected

    def open_tcp(self, ip_address='127.0.0.1', socket=5025):
        self._connected   = True
        self._tcp_address = ip_address
        self._tcp_port    = socket

    @property
    def tcp_address(self):
        return self._tcp_address
    @property
    def tcp_port(self):
        return self._tcp_port

    @property
    def throw_position(self):
        self.assert_connected()
        return self._throw_position
    @throw_position.setter
    def throw_position(self, position):
        self.assert_connected()
        if not Switch.isValidPosition(position):
            self._errors.append((-100, f'Invalid switch position {position}'))
        self._throw_position = position
        print(f'switch position: {position}')

    def is_error(self):
        response = bool(self.errors)
        self.clear_status()
        return response
    @property
    def errors(self):
        return self._errors
    def clear_status(self):
        self._errors.clear()

    def assert_connected(self):
        if not self.connected:
            raise socket.timeout('timed out')
    @staticmethod
    def isValidPosition(position):
        return position <= 6 and position >= 1

def replace():
    import lib.switch
    lib.switch.Switch = Switch
