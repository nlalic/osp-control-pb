from enum import IntEnum

class Page(IntEnum):
    CONNECT  = 0
    CONTROLS = 1
