from rohdeschwarz.instruments.ospswitch import OspSwitch

class Switch(object):
    def __init__(self, instrument, module, switch):
        self.instr = None
        self.address = {
            'instrument': instrument,
            'module': module,
            'switch': switch
        }

    @property
    def connected(self):
        return bool(self.instr)
    def open_tcp(self, ip_address='127.0.0.1', socket=5025):
        try:
            self.instr = OspSwitch()
            self.instr.open_tcp(ip_address, socket)
            if not self.instr.connected():
                self.instr = None
                raise Exception('Could not connect to instrument')
        except Exception as err:
            self.instr = None
            raise err

    @property
    def tcp_address(self):
        return self.instr.bus.getsockname()[0]
    @property
    def tcp_port(self):
        return self.instr.bus.getsockname()[1]

    @property
    def throw_position(self):
        return self.instr.switch_state(self.address)
    @throw_position.setter
    def throw_position(self, position):
        self.instr.close_switch(self.address, position)
