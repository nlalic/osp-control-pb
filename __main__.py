from   lib.controller    import Controller
from   lib.widgets       import MainWindow
from   PySide2.QtWidgets import QApplication
import sys


if __name__ == "__main__":
    app  = QApplication()
    view = MainWindow()
    view.show()

    if '--debug' in [i.lower() for i in sys.argv]:
        import lib.test.switch
        lib.test.switch.replace()
    from lib.switch import Switch
    switch     = Switch(instrument=1, module=1, switch=1)
    
    controller = Controller(view, switch)
    sys.exit(app.exec_())
